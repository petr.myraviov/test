<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'language'],function () {
    Auth::routes();
    Route::get('/', function () {
        return view('welcome');
    });
});

Route::get('language/{locale}', 'LanguageSwitcherController@switcher')
    ->name('language.switcher')
    ->where('locale', 'en|ru');
