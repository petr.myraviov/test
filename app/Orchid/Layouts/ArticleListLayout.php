<?php

namespace App\Orchid\Layouts;

use App\Article;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ArticleListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'articles';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
                TD::set('title', 'Title')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Article $article) {
                        return Link::make($article->title)
                            ->route('platform.articles.edit', $article);
                    }),
                TD::set('created_at', 'Created')->sort(),
                TD::set('updated_at', 'Last edit')->sort(),
        ];
    }
}
