<?php

namespace App\Orchid\Screens;

use App\Article;
use App\Orchid\Layouts\ArticleListLayout;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;

class ArticleListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'All Articles';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All articles description';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'articles' => Article::filters()->defaultSort('id')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
                ->icon('icon-pencil')
                ->route('platform.articles.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            ArticleListLayout::class
        ];
    }
}
