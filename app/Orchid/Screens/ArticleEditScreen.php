<?php

namespace App\Orchid\Screens;

use App\Article;
use App\Http\Requests\ArticleRequest;
use App\User;
use Orchid\Screen\Action;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;

class ArticleEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Create Article';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Mango article';
    public $exists = false;

    /**
     * Query data.
     *
     * @param Article $article
     * @return array
     */
    public function query(Article $article): array
    {
        $this->exists = $article->exists;
        if($this->exists){
            $this->name = 'Edit article';
        }
        return [
            'article' => $article
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create article')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Remove')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('article.title')
                    ->title('Title')
                    ->placeholder('Attractive but mysterious title')
                    ->help('Specify a short descriptive title for this post.'),
                Relation::make('article.user_id')
                    ->title('Author')
                    ->fromModel(User::class, 'name', 'id'),
                Quill::make('article.content')
                    ->title('Main text'),
                Upload::make('image'),
            ])
        ];
    }

    /**
     * @param Article $article
     * @param ArticleRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Article $article, ArticleRequest $request)
    {
        $article->fill($request->get('article'))->save();
        Alert::info('You have successfully created an post.');
        return redirect()->route('platform.articles.list');
    }

    /**
     * @param Article $article
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Article $article)
    {
        $article->delete()
            ? Alert::info('You have successfully deleted the article.')
            : Alert::warning('An error has occurred');
        return redirect()->route('platform.articles.list');
    }
}
